const jwt = require('jsonwebtoken');

const driverMiddleware = (req, res) => {
  const [, token] = req.headers.authorization.split(' ');
  const tokenPayload = jwt.verify(token, 'secret-jwt-key');

  if (tokenPayload.role !== 'DRIVER') {
    return res.status(401).json({ message: 'This action is available for drivers only' });
  }

  return res.json();
};

module.exports = {
  driverMiddleware,
};
