const jwt = require('jsonwebtoken');

// const truckTypes = ['SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT'];

const shipperMiddleware = (req, res) => {
  const [, token] = req.headers.authorization.split(' ');
  const tokenPayload = jwt.verify(token, 'secret-jwt-key');

  if (tokenPayload.role !== 'SHIPPER') {
    return res.status(401).json({ message: 'This action is available for shippers only' });
  }
  return res.json();
};

module.exports = {
  shipperMiddleware,
};
