const mongoose = require('mongoose');
const Joi = require('joi');

const userJoiSchema = Joi.object({
  email: Joi.string()
    .email({ minDomainSegments: 2, tlds: { allow: ['com', 'net'] } })
    .required(),

  password: Joi.string()
    .pattern(('^[a-zA-Z0-9]{3,30}$'))
    .required(),

  role: Joi.string()
    .min(6)
    .max(7)
    .required(),
});

const User = mongoose.model('User', {
  role: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
    unique: true,
  },
  password: {
    type: String,
    required: true,
  },
  created_date: {
    type: String,
  },
});

module.exports = {
  User,
  userJoiSchema,
};
