const mongoose = require('mongoose');

const loadSchema = mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  payload: {
    type: Number,
    required: true,
  },
  pickup_address: {
    type: String,
    required: true,
  },
  delivery_address: {
    type: String,
    required: true,
  },
  dimensions: {
    width: Number,
    length: Number,
    height: Number,
  },
  created_by: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  },
  assigned_to: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  },
  statusLoad: {
    type: String,
    required: true,
  },
  state: {
    type: String,
    required: true,
  },
  created_date: {
    type: String,
  },
});

const Load = mongoose.model('load', loadSchema);

module.exports = {
  Load,
};
