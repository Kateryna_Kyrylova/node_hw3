const express = require('express');

const router = express.Router();
const {
  createTruck,
  getTruck,
  getTrucks,
  updateTruck,
  deleteTruck,
  assignTruck,
} = require('./trucksService');

const { authMiddleware } = require('./middleware/authMiddleware');
const { driverMiddleware } = require('./middleware/driverMiddleware');

router.post('/', authMiddleware, driverMiddleware, createTruck);

router.post('/:id/assign', authMiddleware, driverMiddleware, assignTruck);

router.get('/', authMiddleware, driverMiddleware, getTrucks);

router.get('/:id', authMiddleware, driverMiddleware, getTruck);

router.put('/:id', authMiddleware, driverMiddleware, updateTruck);

router.delete('/:id', authMiddleware, driverMiddleware, deleteTruck);

module.exports = {
  trucksRouter: router,
};
