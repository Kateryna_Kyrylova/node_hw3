const jwt = require('jsonwebtoken');
const { User } = require('./models/Users');

const getProfileInfo = (req, res) => {
  const [, token] = req.headers.authorization.split(' ');
  const tokenPayload = jwt.verify(token, 'secret-jwt-key');

  User.findById(tokenPayload.userId)
    .then((userInfo) => {
      res.json(userInfo);
    });
};

const deleteProfile = (req, res) => {
  const [, token] = req.headers.authorization.split(' ');
  const tokenPayload = jwt.verify(token, 'secret-jwt-key');

  User.findByIdAndDelete(tokenPayload.userId)
    .then(() => {
      res.json('Profile deleted successfully');
    });
};

module.exports = {
  getProfileInfo,
  deleteProfile,
};
