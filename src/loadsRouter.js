const express = require('express');

const router = express.Router();
const {
  createLoad,
  getLoads,
  getLoad,
  updateLoad,
  deleteLoad,
  // getMyLoads,
  // updateMyLoadById,
  // markMyLoadCompletedById,
} = require('./loadsService');

const { authMiddleware } = require('./middleware/authMiddleware');
const { shipperMiddleware } = require('./middleware/shipperMiddleware');

router.post('/', authMiddleware, createLoad);

router.get('/', authMiddleware, shipperMiddleware, getLoads);

router.get('/:id', authMiddleware, shipperMiddleware, getLoad);

router.put('/:id', authMiddleware, shipperMiddleware, updateLoad);

router.delete('/:id', authMiddleware, shipperMiddleware, deleteLoad);

// router.get('/get-my-notes', authMiddleware, getMyLoads);

// router.put('/:id', authMiddleware, updateMyLoadById);

// router.patch('/mark-completed/:id', authMiddleware, markMyLoadCompletedById);

module.exports = {
  loadsRouter: router,
};
