const express = require('express');

const router = express.Router();
const { getProfileInfo, deleteProfile } = require('./usersService');

router.get('/me', getProfileInfo);

router.delete('/me', deleteProfile);

module.exports = {
  usersRouter: router,
};
