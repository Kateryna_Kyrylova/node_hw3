const dayjs = require('dayjs');
const jwt = require('jsonwebtoken');
const { Truck } = require('./models/Trucks');

function createTruck(req, res) {
  const { type } = req.body;
  const [, token] = req.headers.authorization.split(' ');
  const tokenPayload = jwt.verify(token, 'secret-jwt-key');

  const createdDate = dayjs().format('YYYY-MM-DDTHH:mm:ss');
  const assignedTo = null;
  const status = 'IS';
  const createdBy = tokenPayload.userId;

  console.log(createdBy);

  const truck = new Truck({
    createdBy,
    assignedTo,
    type,
    status,
    createdDate,
  });

  truck.save()
    .then((saved) => {
      res.json(saved);
    });
}

function getTrucks(req, res) {
  return Truck.find().then((result) => {
    res.json(result);
  });
}

const getTruck = (req, res) => Truck.findById(req.params.id)
  .then((truck) => {
    res.json(truck);
  });

const updateTruck = async (req, res) => {
  const truck = await Truck.findById(req.params.id);
  const { text } = req.body;

  if (text) truck.text = text;

  return truck.save().then((saved) => res.json(saved));
};

const deleteTruck = (req, res) => Truck.findByIdAndDelete(req.params.id)
  .then((truck) => {
    res.json(truck);
  });

const assignTruck = async (req, res) => {
  const truck = await Truck.findById(req.params.id);
  const { assignedTo } = req.body;

  if (assignedTo) truck.assigned_to = assignedTo;

  return truck.save().then((saved) => res.json(saved));
};

module.exports = {
  createTruck,
  getTruck,
  getTrucks,
  updateTruck,
  deleteTruck,
  assignTruck,
};
