const { Load } = require('./models/Loads');

function createLoad(req, res) {
  const {
    name, payload, deliveryAddress, pickupAddress, dimensions, createdBy,
    assignedTo, state,
  } = req.body;
  const statusLoad = 'NEW';
  const load = new Load({
    name,
    payload,
    deliveryAddress,
    pickupAddress,
    dimensions,
    createdBy,
    assignedTo,
    statusLoad,
    state,
  });
  load.save()
    .then((saved) => {
      res.json(saved);
    });
}

function getLoads(req, res) {
  return Load.find().then((result) => {
    res.json(result);
  });
}

const getLoad = (req, res) => Load.findById(req.params.id)
  .then((load) => {
    res.json(load);
  });

const updateLoad = async (req, res) => {
  const load = await Load.findById(req.params.id);
  const { text } = req.body;

  if (text) load.text = text;

  return load.save().then((saved) => res.json(saved));
};

const deleteLoad = (req, res) => Load.findByIdAndDelete(req.params.id)
  .then((load) => {
    res.json(load);
  });

module.exports = {
  createLoad,
  getLoad,
  getLoads,
  updateLoad,
  deleteLoad,
};
