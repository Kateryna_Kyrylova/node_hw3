const jwt = require('jsonwebtoken');
const bcryptjs = require('bcryptjs');
const dayjs = require('dayjs');
const { User, userJoiSchema } = require('./models/Users');

const saveUser = async ({
  role, email, password, createdDate,
}) => {
  const user = new User({
    role,
    email,
    password: await bcryptjs.hash(password, 10),
    createdDate,
  });

  return user.save();
};

const registerUser = async (req, res) => {
  const { role, email, password } = req.body;
  const createdDate = dayjs().format('YYYY-MM-DDTHH:mm:ss');
  await userJoiSchema.validateAsync({ role, email, password });

  const user = await saveUser({
    role, email, password, createdDate,
  });
  return res.json(user);
};

const loginUser = async (req, res) => {
  const user = await User.findOne({ email: req.body.email });
  if (user && await bcryptjs.compare(String(req.body.password), String(user.password))) {
    // eslint-disable-next-line no-underscore-dangle
    const payload = { email: user.email, role: user.role, userId: user._id };
    const jwtToken = jwt.sign(payload, 'secret-jwt-key');
    return res.json({ message: 'Success', token: jwtToken });
  }
  return res.status(403).json({ message: 'Not authorized' });
};

const forgotPassword = (req, res) => {
  const [, token] = req.headers.authorization.split(' ');
  const tokenPayload = jwt.verify(token, 'secret-jwt-key');

  User.findByIdAndUpdate(tokenPayload.userId)
    .then(() => {
      res.json('New password sent to your email address');
    });
};

module.exports = {
  registerUser,
  loginUser,
  forgotPassword,
};
