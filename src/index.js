require('dotenv').config();

const fs = require('fs');
const express = require('express');
const morgan = require('morgan');
const mongoose = require('mongoose');

const app = express();

mongoose.connect('mongodb+srv://admin:admin123@katerynakyrylovamongodb.4rljmgg.mongodb.net/UberTracks?retryWrites=true&w=majority');

const { loadsRouter } = require('./loadsRouter');
const { trucksRouter } = require('./trucksRouter');
const { usersRouter } = require('./usersRouter');
const { authRouter } = require('./authRouter');

app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/loads', loadsRouter);
app.use('/api/trucks', trucksRouter);
app.use('/api/users', usersRouter);
app.use('/api/auth', authRouter);

const port = process.env.PORT || 8080;

const start = async () => {
  try {
    if (!fs.existsSync('UberTracks')) {
      fs.mkdirSync('UberTracks');
    }
    app.listen(port);
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
};

start();

// ERROR HANDLER

function errorHandler(err, req, res) {
  console.error('err');
  res.status(500).send({ message: 'Server error' });
}

app.use(errorHandler);
